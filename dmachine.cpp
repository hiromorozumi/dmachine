#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>
#include <unistd.h>
#include <cstdlib>

#if defined (_WIN32)
	#include "portaudio_win32/portaudio.h"
	#include "libsndfile_win32/sndfile.h"
#elif defined (__APPLE__) || (__linux__)
	#include "portaudio.h"
	#include "sndfile.h"
#endif

#include "hirolib.h"

using namespace std;

#if defined (_WIN32)
	const std::string SH = "\\";
#elif defined (__APPLE__) || (__linux__)
	const std::string SH = "/";
#endif


struct Note
{
	int measure;
	int beat;
	int displacement;
	int velocity;
	
	long startPos;
	long len;
};

////////////////////////////////
// struct to use for storage
////////////////////////////////

struct Pattern
{
	std::string name;
	Note notes[128][8];
	int nNotes[8];
	int sampleID[8];
	std::string sampleName[8];
	int nTracks;
	int defaultBPM;
	int nBeats;
	int nMeasures;
};


////////////////////


class Sample
{
public:
	Sample()
	{
		filename = "";
		data.resize(1);
		dataLen = 1;
		loaded = false;
	}
	// ~Sample();
	
	std::string filename;
	vector<float> data;
	int dataLen;
	bool loaded;
};

class Channel
{
public:

	Channel()
	{
		initialize();
	}
	
	Sample* sample;

	Note notes[128];
	int nNotes;
	std::string sampleName;
	int sampleID;
	
	bool active;
	bool muted;
	bool samplePlaying;
	bool reachedEnd;
	int currentNote;
	int nextNote;
	float gain;
	
	int posInSample;

	int posInPattern;
	
	void initialize();
	void setPosInPattern(int posInPtn);
	void setUpAtBeginning();
	void checkAndSetUpNewNote();
	void startSample();
	void stopSample();
	void reset();
	bool isActive();
	bool isMuted();
	bool sampleIsPlaying();
	void setGain(float g);
	float getGain();
	float process();
};

void Channel::initialize()
{
	active = true;
	muted = false;
	samplePlaying = false;
	reachedEnd = false;
	currentNote = 0;
	gain = 1.0f;
	posInSample = 0;
	posInPattern = 0;
}

void Channel::setPosInPattern(int posInPtn)
{
	posInPattern = posInPtn;
}

void Channel::setUpAtBeginning()
{
	active = true;
	currentNote = 0;
	
	if(nNotes<=1)
		nextNote = 0; // if there's only one note, always currentNote = nextNote = 0
	else
		nextNote = 1;
	
	// gain = 1.0f;
	setGain( (float) notes[0].velocity / 100.0f );
	posInSample = 0;
	posInPattern = 0;
	
	// should start note #0 right away for this channel!
	if(notes[0].startPos==0)
		startSample();
	else
		stopSample();
}

void Channel::checkAndSetUpNewNote()
{
	// if current note index is 0 and this track has delayed start, and just now reached this note
	if(currentNote==0 && (notes[0].startPos>0) && (posInPattern == notes[0].startPos ) && !samplePlaying)
	{
		if(notes[currentNote].velocity==0) // if velocity 0 is requested, we'll explicitly do rest
			stopSample();
		// otherwise, we're good to start this new note
		else
		{	
			setGain( (float) notes[currentNote].velocity / 100.0f );
			startSample();
		}
	}
	
	// otherwise, check if we've reached the next note's starting position
	if( posInPattern == notes[nextNote].startPos )
	{
		currentNote = nextNote;
		
		nextNote++;
		if(nextNote >= nNotes)
			nextNote = 0;
		
		if(notes[currentNote].velocity==0) // if velocity 0 is requested, we'll explicitly do rest
			stopSample();
		// otherwise, we're good to start this new note
		else
		{	
			setGain( (float) notes[currentNote].velocity / 100.0f );
			startSample();
		}
	}
	
	
	// DEBUG
	if(posInPattern > notes[nextNote].startPos && (nNotes > 1) && ((currentNote < nNotes-1)) )
	{
		currentNote = nextNote;
		
		nextNote++;
		if(nextNote >= nNotes)
			nextNote = 0;
		
		if(notes[currentNote].velocity==0) // if velocity 0 is requested, we'll explicitly do rest
			stopSample();
		// otherwise, we're good to start this new note
		else
		{	
			setGain( (float) notes[currentNote].velocity / 100.0f );
			startSample();
		}			
			
		cout << "(OVERFLOW->resucued!)\r\n";
	}
}

void Channel::startSample()
{
	posInSample = 0;
	samplePlaying = true;
	reachedEnd = false;
}

void Channel::stopSample()
{
	reachedEnd = true;
	samplePlaying = false;
}

void Channel::reset()
{}

bool Channel::isActive()
{
	return active;
}

bool Channel::isMuted()
{
	return muted;
}

bool Channel::sampleIsPlaying()
{
	return samplePlaying;
}

void Channel::setGain(float g)
{
	if(g < 0.0f) g = 0.0f;
	else if(g > 1.0f) g = 1.0f;
	gain = g;
}

float Channel::getGain()
{
	return gain;
}

float Channel::process()
{
	if(!active) return 0.0f;
	
	float out = 0.0f;
	
	if(!reachedEnd && samplePlaying)
	{
		out = sample->data[posInSample] * gain;
		
		// hard limit
		if(out < -1.0f)
			out = -0.99f;
		else if(out > 1.0f)
			out = 1.0f;
		
		// advance index
		posInSample++;
		
		if(posInSample >= (sample->dataLen))
		{
			reachedEnd = true;
			samplePlaying = false;
		}
	}
	
	return out;
	
}

////////////////

class DMachine
{
public:

	static const double SAMPLE_RATE;

	DMachine()
	{	
		callBackExecuting= false;
		gain = 0.7f;
	}
	
	vector<Pattern> patterns;
	Sample samples[16];
	float readBuffer[4096];
	Channel ch[16];
	std::string strData;
	
	bool callBackExecuting; // this gets changed by Audio's callback function
	
	int nPatterns;
	int nChannelsUsed;
	int currentPattern;
	std::string currentPatternName;
	int measure;
	int beat;
	double displacement;
	int beatLen;
	int nBeats;
	int nMeasures;
	int pos;
	int patternLen;
	double bpm;
	bool playing;
	float gain;
	
	void loadAllSamples(const std::string& dirPath);
	void loadSample(int slot, const std::string& filepath);
	void loadPatterns(std::string filepath);
	int getSampleID(const std::string &sampleName);
	void setBPM(int beatsPerMinute);
	int getBPM();
	void setGain(float g);
	float getGain();
	void setPattern(int patternNum);
	int getCurrentPattern();
	std::string getCurrentPatternName();
	int getTotalNumberOfPatterns();
	int getTotalNumberOfChannelsUsed();
	void remapChannels();
	void goToBeginning();
	void start();
	void pause();
	bool isPlaying();
	float getMix();
	float compress(float in);
	void initialize();
	
	std::string reportChannel(int chnl);
	std::string reportPlayerStats();
	std::string reportPattern(int chnl);
	std::string reportSamples();
};

const double DMachine::SAMPLE_RATE = 44100.0;

// load all sound sample files
void DMachine::loadAllSamples(const std::string& dirPath)
{
	/*
	loadSample(0, string(dirPath + SH + "ACOUSTIC_BD_1.wav"));		// 0 - bass drum
	loadSample(1, string(dirPath + SH + "ACOUSTIC_SN_1.wav"));		// 1 - snare
	loadSample(2, string(dirPath + SH + "ACOUSTIC_HH_CL_1.wav"));	// 2 - hihat closed
	loadSample(3, string(dirPath + SH + "ACOUSTIC_TM_HI_1.wav"));	// 3 - tom hi
	loadSample(4, string(dirPath + SH + "ACOUSTIC_TM_LO_1.wav"));	// 4 - tom low
	loadSample(5, string(dirPath + SH + "ACOUSTIC_RD_1.wav"));		// 5 - ride cymbal
	loadSample(6, string(dirPath + SH + "ACOUSTIC_CR_1.wav"));		// 6 - crash cymbal
	loadSample(7, string(dirPath + SH + "ACOUSTIC_SN_GST_1.wav"));	// 7 - snare ghost
	loadSample(8, string(dirPath + SH + "ACOUSTIC_HH_OP_1.wav"));	// 8 - hihat open
	loadSample(9, string(dirPath + SH + "ACOUSTIC_RM_1.wav"));		// 9 - rim shot
	*/
	
	vector<string> sampleFilePaths;
	vector<string> tempPaths = file::getFileNamesInDir(dirPath);
	int nFilesExisting = tempPaths.size();
	
	if(nFilesExisting > 16)
		nFilesExisting = 16;
	
	for(int i=0; i<nFilesExisting; i++)
	{
		if(tempPaths[i].find(".wav")!=string::npos)
		{
			string fullPath = dirPath + SH + tempPaths[i];
			sampleFilePaths.push_back(fullPath);
		}
	}
	
	for(int i=0; i<sampleFilePaths.size(); i++)
	{
		loadSample(i, sampleFilePaths[i]);
	}
}

// load up a drum sound to a specified slot
void DMachine::loadSample(int slot, const std::string& filepath)
{
	// open sound file
	SF_INFO sndInfo;
	SNDFILE *sndFile = sf_open(filepath.c_str(), SFM_READ, &sndInfo);
	if(sndFile==NULL)
	{
		cout << "Error reading file: " + filepath;
		return;
	}
	
	// check format
	if( (sndInfo.format != (SF_FORMAT_WAV | SF_FORMAT_PCM_16) &&
		(sndInfo.format != (SF_FORMAT_OGG | SF_FORMAT_VORBIS) ) ))
		{
			cout << "Wrong format: " + filepath;
			return;
		}
		
	// register the sample file name
	string nameToRegister = filepath;
	if(filepath.length()>1)
	{
		int beginPos = filepath.find_last_of(SH);
		beginPos++;
		nameToRegister = filepath.substr(beginPos);	
	}
	samples[slot].filename = nameToRegister;	
	
	// clear the sound data entirely
	samples[slot].data.resize(0);
	
	int posToStore = 0;
	bool readDone = false;
	int framesRead;
	
	// read the file into the data
	while(!readDone)
	{
		framesRead = sf_readf_float(sndFile, readBuffer, 4092); // we'll read 4092 samples - mono
		if(framesRead > 0) // if zero, we're at the end already
		{
			for(int i=0; i<framesRead; i++)
			{
				if(posToStore<88200) // 2 seconds max
				{
					samples[slot].data.push_back(readBuffer[i]);
					posToStore++;
				}
			}
		}
		else // zero returned, we're done now 
			readDone = true;
	}
	
	if(samples[slot].data.size()==0)
	{
		samples[slot].data.push_back(0);	
	}
	else
		samples[slot].loaded = true;
	
	samples[slot].dataLen = samples[slot].data.size();
	sf_close(sndFile);
	
	// DEBUG
	// cout << "slot = " << slot << "\r\n";
	// cout << "datasize = " << samples[slot].data.size() << "\r\n";
	
}



void DMachine::loadPatterns(std::string filepath)
{
	strData = "";
	ifstream inFile;
	inFile.open(filepath.c_str());
	string line;
	size_t found;
	
	if(inFile.is_open())
	{
		while(getline(inFile, line) )
		{
			// strip down comment
			found = line.find("//");
			if(found!=string::npos)
				line = line.substr(0, found);
			strData += line + "   ";
		}
		inFile.close();
	}
	else
		cout << "Unable to open file: " << filepath << "\r\n";
		
	// safeguarding...
	strData += "                $$$";
		
	// first, separate by each pattern
	vector<string> strPatterns;
	bool patternSplitDone = false;
	while(!patternSplitDone)
	{
		int startPos = strData.find("BEGIN_PATTERN");
		int endPos = strData.find("END_PATTERN");
		
		// if both PATTERN and PATTERN_END are found, then add a pattern
		if(startPos!=string::npos && endPos!=string::npos)
		{	
			endPos += 11; // last char of "PATTERN_END"
			int nCharsToErase = endPos - startPos;
			string strThisPattern = strData.substr(startPos, nCharsToErase);
			strPatterns.push_back(strThisPattern);
			strData.erase(startPos, nCharsToErase);
		}
		else
			patternSplitDone = true;
	}
	
	// first, expand vector of patterns
	nPatterns = (int)strPatterns.size();
	patterns.clear();
	patterns.resize(nPatterns);
	
	// fill note data with -1 to default
	for(int i=0; i<nPatterns; i++)
	{
		for(int j=0; j<8; j++)
		{
			for(int k=0; k<64; k++)
			{
				patterns[i].notes[k][j].measure = -1;
				patterns[i].notes[k][j].beat = -1;
				patterns[i].notes[k][j].displacement = 0;
				patterns[i].notes[k][j].velocity = 0;
				patterns[i].notes[k][j].startPos = 0;
				patterns[i].notes[k][j].len = 0;
			}
		}
	}
	
	// for each pattern... extract track info
	for(int i=0; i<strPatterns.size(); i++)
	{
		bool patternDone = false;
		string str = strPatterns[i];

		// extract name
		found = str.find("NAME='");
		if(found!=string::npos)
		{
			string strName = str.substr(found+6, 30);
			found = strName.find_last_of("'");
			if(found!=string::npos)
				strName = strName.substr(0, found);
			
			// register the pattern name
			patterns[i].name = strName;
		}
		
		// extract number of tracks
		found = str.find("N_TRACKS=");
		if(found!=string::npos)
		{
			string strNTracks = str.substr(found+9, 1);
			patterns[i].nTracks = atoi(strNTracks.c_str());
		}
		
		// extract default bpm
		found = str.find("BPM=");
		if(found!=string::npos)
		{
			string strBPM = str.substr(found+4,3);
			patterns[i].defaultBPM = atoi(strBPM.c_str());
		}
		
		// extract number of beats
		found = str.find("N_BEATS=");
		if(found!=string::npos)
		{
			string strNBeats = str.substr(found+8, 2);
			patterns[i].nBeats = atoi(strNBeats.c_str());			
		}
		
		// extract number of measures
		found = str.find("N_MEASURES=");
		if(found!=string::npos)
		{
			string strNMeasures = str.substr(found+11, 1);
			patterns[i].nMeasures = atoi(strNMeasures.c_str());
		}
	
		
		// now extract track data one by one
		bool allTracksDone = false;
		int currentTrack = 0;
		while(!allTracksDone)
		{
			int startPosTrack = str.find("BEGIN_TRACK");
			int endPosTrack = str.find("END_TRACK");
			
			if(startPosTrack!=string::npos && endPosTrack!=string::npos)
			{
				endPosTrack += 9;
				int nCharsToExtract = endPosTrack - startPosTrack;
				string strTrack = str.substr(startPosTrack, nCharsToExtract);
				strTrack += "                 "; // safeguarding
				str.erase(startPosTrack, nCharsToExtract);
				
				// first determine the sample to use
				string sampleName = "DEFAULT";
				int startPosSample = strTrack.find("SAMPLE='");
				int endPosSample = strTrack.find("'", startPosSample+8);
				if(startPosSample!=string::npos && endPosSample!=string::npos)
				{
					nCharsToExtract = (endPosSample) - (startPosSample+8);
					sampleName = strTrack.substr(startPosSample+8, nCharsToExtract);
					if(sampleName.length()>32)
						sampleName = sampleName.substr(0,32);
				}
				
				// get the sampleID from the string
				patterns[i].sampleName[currentTrack] = sampleName;
				patterns[i].sampleID[currentTrack] = getSampleID(sampleName);
				
				// iterate through... read all notes in this track
				int totalNotesRead = 0;
				int currentNote = 0;
				int readPos = 0;
				int strTrackLen = strTrack.length();
				
				bool allNotesDone = false;
				while(!allNotesDone)
				{
					int noteStartPos = strTrack.find("NOTE[", 0);
					int noteEndPos = strTrack.find(']', noteStartPos);
					
					if( noteStartPos!=string::npos && noteEndPos!=string::npos )
					{
						noteEndPos += 1;
						int nCharsToErase = noteEndPos - noteStartPos;
						int extractStart = noteStartPos + 5;
						int nCharsToExtract = nCharsToErase - 6; // exclude "NOTE[" (5 chars) then "]" char (1 char)
						string strNote = strTrack.substr(extractStart, nCharsToExtract);
						strTrack.erase(noteStartPos, nCharsToErase);
						
						int readStart = 0;
						int measureNumToStore = -1;
						int beatNumToStore = -1;
						int displacementToStore = 0;
						int velocityToStore = 0;
						
						// get measure no.
						found = strNote.find(",", 0);
						if(found!=string::npos)
						{
							measureNumToStore = atoi( strNote.substr(readStart, (found-readStart)).c_str() );
							readStart = found+1;
							
							// get beat no.
							found = strNote.find(",", readStart);
							if(found!=string::npos)
							{
								beatNumToStore = atoi( strNote.substr(readStart, (found-readStart)).c_str() );
								readStart = found+1;
								
								// get displacement value
								found = strNote.find(",", readStart);
								if(found!=string::npos)
								{
									displacementToStore = atoi( strNote.substr(readStart, (found-readStart)).c_str() );
									readStart = found+1;
									
									// get velocity
									velocityToStore = atoi( strNote.substr(readStart).c_str() );
								}
							}	
						}
						
						// finally, register all these values
						patterns[i].notes[currentNote][currentTrack].measure = measureNumToStore;
						patterns[i].notes[currentNote][currentTrack].beat = beatNumToStore;
						patterns[i].notes[currentNote][currentTrack].displacement = displacementToStore;
						patterns[i].notes[currentNote][currentTrack].velocity = velocityToStore;
						patterns[i].notes[currentNote][currentTrack].startPos = 0;
						patterns[i].notes[currentNote][currentTrack].len = 0;
						
						currentNote++;
						totalNotesRead++;
					}
					else
					{
						patterns[i].nNotes[currentTrack] = totalNotesRead;
						allNotesDone = true;
					}
				}
				
				currentTrack++; // will read next track in next loop iteration
			}
			else
				allTracksDone = true;
		}
		
	}

	// determine how many patterns we've read
	nPatterns = (int)patterns.size();
}

int DMachine::getSampleID(const std::string &sampleName)
{
	int ret = -1;
	
	/*
	if(sampleName=="DEFAULT") ret = -1;
	else if(sampleName=="ACOUSTIC_BD_1") 		ret = 0;
	else if(sampleName=="ACOUSTIC_SN_1") 		ret = 1;
	else if(sampleName=="ACOUSTIC_HH_CL_1") 	ret = 2;
	else if(sampleName=="ACOUSTIC_TM_HI_1") 	ret = 3;
	else if(sampleName=="ACOUSTIC_TM_LO_1") 	ret = 4;
	else if(sampleName=="ACOUSTIC_RD_1") 		ret = 5;
	else if(sampleName=="ACOUSTIC_CR_1")	 	ret = 6;
	else if(sampleName=="ACOUSTIC_SN_GST_1")	ret = 7;
	else if(sampleName=="ACOUSTIC_HH_OP_1") 	ret = 8;
	else if(sampleName=="ACOUSTIC_RM_1") 		ret = 9;
	*/
	
	for(int i=0; i<16; i++)
	{
		if(samples[i].filename.find(string(sampleName+".wav"))!=string::npos)
			ret = i;
	}
	
	return ret;
}

void DMachine::setBPM(int beatsPerMinute)
{
	// if Audio class's callback is in action, wait til it's done
	// while(callBackExecuting){}
	
	bpm = beatsPerMinute;
	beatLen = (int)(SAMPLE_RATE * 60.0 / (double)bpm);
	
	// then you'll know the patternLen
	patternLen = nBeats * nMeasures * beatLen;
	
	// adjust the pos to new scale for this BPM
	int newPos = beat * measure * beatLen + (int)( (displacement / 10000.0) * beatLen);
	newPos -= 4; // safeguarding... back up just a little bit (account for rounding error in calculation)
	
	// if pos overflowed... go to beginning
	if(newPos>patternLen || newPos<0)
		goToBeginning();
}

int DMachine::getBPM()
{
	return bpm;
}

void DMachine::setGain(float g)
{
	gain = g;
}

float DMachine::getGain()
{
	return gain;
}

void DMachine::setPattern(int patternNum)
{
	currentPattern = patternNum;
	currentPatternName = patterns[currentPattern].name;
	int ptn = currentPattern;
	nChannelsUsed = patterns[ptn].nTracks;
	
	// set up the samples called for this pattern
	for(int j=0; j<patterns[ptn].nTracks; j++)
	{
		ch[j].sampleName = patterns[ptn].sampleName[j];
		ch[j].sampleID = patterns[ptn].sampleID[j];	
		
		// set the right pointer to sample data for each channel
		if( ch[j].sampleID >= 0 && ch[j].sampleID < 16 )
		{
			ch[j].sample = &samples[ ch[j].sampleID ];
		}
	}

	// set up nBeats / nMeasures according to current pattern
	nBeats = patterns[currentPattern].nBeats;
	nMeasures = patterns[currentPattern].nMeasures;	
	
	// set up the default bpm
	setBPM(patterns[currentPattern].defaultBPM);
	
	// then you'll know the patternLen
	patternLen = nBeats * nMeasures * beatLen;

	// iterate through each track within this pattern
	// and copy each note info
	for(int j=0; j<patterns[ptn].nTracks; j++)
	{
		ch[j].nNotes = patterns[ptn].nNotes[j];
		for(int k=0; k<patterns[ptn].nNotes[j]; k++)
		{
			ch[j].notes[k].measure = patterns[ptn].notes[k][j].measure;
			ch[j].notes[k].beat = patterns[ptn].notes[k][j].beat;
			ch[j].notes[k].displacement = patterns[ptn].notes[k][j].displacement;
			ch[j].notes[k].velocity = patterns[ptn].notes[k][j].velocity;
			
			// calculate this note's position...
			ch[j].notes[k].startPos = ( (ch[j].notes[k].measure * nBeats) + ch[j].notes[k].beat ) * beatLen 
					+ (int)(  beatLen * ( (double)ch[j].notes[k].displacement / 10000.0) );
		}
	}	
}

int DMachine::getCurrentPattern()
{
	return currentPattern;	
}

std::string DMachine::getCurrentPatternName()
{
	return currentPatternName;	
}

int DMachine::getTotalNumberOfPatterns()
{
	return nPatterns;
}

int DMachine::getTotalNumberOfChannelsUsed()
{
	return nChannelsUsed;
}

// remap the currently set pattern data to channels
// timing will be adjusted according to current BPM
void DMachine::remapChannels()
{
	int ptn = currentPattern;

	// iterate through each track within this pattern
	// and REMAP the note position info
	for(int j=0; j<patterns[ptn].nTracks; j++)
	{
		ch[j].nNotes = patterns[ptn].nNotes[j];
		for(int k=0; k<patterns[ptn].nNotes[j]; k++)
		{			
			// calculate this note's position...
			ch[j].notes[k].startPos = ( (ch[j].notes[k].measure * nBeats) + ch[j].notes[k].beat ) * beatLen 
					+ (int)(  beatLen * ( (double)ch[j].notes[k].displacement / 10000.0) );
		}
	}

}

void DMachine::goToBeginning()
{
	pos = 0;
	for(int j=0; j<nChannelsUsed; j++)
		ch[j].setUpAtBeginning();
}

float DMachine::getMix()
{
	float out = 0.0f;
	if(playing)
	{
		// check for each channel - is it time to start a new note?
		for(int j=0; j<nChannelsUsed; j++)
		{
			ch[j].setPosInPattern(pos);
			ch[j].checkAndSetUpNewNote();
		}
		
		// now get output from each channel
		// (this will automatically advance each channel's sample position)
		for(int j=0; j<nChannelsUsed; j++)
		{
			out += ch[j].process();
		}
		
		// apply gain
		out *= gain;
		
		// hard limit
		if(out < -1.0f)
			out = -0.99f;
		else if(out > 1.0f)
			out = 0.99f;
		
		pos++;
	
		// calculate the current measure and beat no.
		measure = pos / (beatLen * nBeats);
		beat = (pos / beatLen) % nBeats;
	
		// calculate the displacement
		displacement = ( ( (double)( pos % beatLen) / (double)beatLen) * 10000.0 );
		
		if(pos >= patternLen)
			pos = 0;
			
	}
	return out;
}

float DMachine::compress(float in)
{
	float out = in;
	float threshold = 0.62f;
	float ratio = 0.25f;
	
	if(in > threshold)
	{
		out = threshold + (in - threshold) * ratio;
	}
	else if(in < -threshold)
	{
		out = -threshold + ( (in + threshold) * ratio);
	}
	return out;
}

void DMachine::initialize()
{
	nPatterns = 0;
	nChannelsUsed = 0;
	nBeats = 0;
	nMeasures = 0;
	currentPattern = 0;
	measure = 0;
	beat = 0;
	pos = 0;
	bpm = 120;
	playing = false;
	patternLen = 0;
	
	// just to be safe... set the pointer to sample
	// for each channel to default
	for(int j=0; j<16; j++)
		ch[j].sample = &samples[j];
}

// DEBUG
std::string DMachine::reportChannel(int chnl)
{
	string str;
	str  = "\r\n";
	str += "******** CHANNEL " + toStr(chnl) + " REPORT ********" + "\r\n" + "\r\n";
	
	str += "Pattern number = " + toStr(currentPattern) + "\r\n";
	str += "BPM = " + toStr(bpm) + "\r\n";
	str += "beatLen = " + toStr(beatLen) + "\r\n";
	str += "nMeasures = " + toStr(nMeasures) + "\r\n";
	str += "nBeats = " + toStr(nBeats) + "\r\n";
	str += "patternLen = " + toStr(patternLen) + "\r\n";
	str += "\r\n";
		
		str += "CHANNEL " + toStr(chnl) + "\r\n\r\n";
		str += "  SAMPLE NAME = " + ch[chnl].sampleName + "\r\n";
		str += "  SAMPLE ID   = " + toStr(ch[chnl].sampleID) + "\r\n";
		str += "\r\n";
		
		for(int k=0; k<ch[chnl].nNotes; k++)
		{
			str += "  N " + toStr(k);
			str += "\t" + toStr(ch[chnl].notes[k].measure);
			str += "\t" + toStr(ch[chnl].notes[k].beat);
			str += "\t" + toStr(ch[chnl].notes[k].startPos);
			str += "\t" + toStr(ch[chnl].notes[k].velocity);
			str += "\r\n";
		}
		str += "\r\n";
		
	str += "\r\n";

	return str;
}

std::string DMachine::reportPlayerStats()
{
	string str;
	
	str  = "\r\n";
	str += "pos                ... " + toStr(pos) + "\r\n";
	str += "measure            ... " + toStr(measure) + "\r\n";
	str += "beat               ... " + toStr(beat) + "\r\n";
	str += "displacement       ... " + toStr(displacement) + "\r\n";
	str += "bpm                ... " + toStr(bpm) + "\r\n";
	str += "beatLen            ... " + toStr(beatLen) + "\r\n";
	str += "patternLen         ... " + toStr(patternLen) + "\r\n";
	str += "currentPattern     ... " + toStr(currentPattern) + "\r\n";
	str += "currentPatternName ... " + currentPatternName + "\r\n";
	str += "nPatterns          ... " + toStr(nPatterns) + "\r\n";
	str += "nChannelsUsed      ... " + toStr(nChannelsUsed) + "\r\n";
	str += "nBeats             ... " + toStr(nBeats) + "\r\n";
	str += "nMeasures          ... " + toStr(nMeasures) + "\r\n";
	str += "gain               ... " + toStr(gain) + "\r\n";
	str += "\r\n";

	return str;
}

std::string DMachine::reportPattern(int ptnNumber)
{
	string str;
	str = "\r\n\r\n";

		str += "***** PATTERN " + toStr(ptnNumber) + " ***** "+ "\r\n" + "\r\n";
		
		str += "Name:       " + patterns[ptnNumber].name + "\r\n";
		str += "nTracks:    " + toStr(patterns[ptnNumber].nTracks) + "\r\n";
		str += "defaultBPM: " + toStr(patterns[ptnNumber].defaultBPM) + "\r\n";
		str += "nBeats:     " + toStr(patterns[ptnNumber].nBeats) + "\r\n";
		str += "nMeasures:  " + toStr(patterns[ptnNumber].nMeasures) + "\r\n";	
		str += "\r\n";
		
		for(int j=0; j<patterns[ptnNumber].nTracks; j++)
		{
			str += "TRACK " + toStr(j) + "\r\n" + "\r\n";
			
			str += "  SAMPLE NAME = " + patterns[ptnNumber].sampleName[j] + "\r\n";
			str += "  SAMPLE ID =   " + toStr(patterns[ptnNumber].sampleID[j]) + "\r\n" + "\r\n";
			
			for(int k=0; k<patterns[ptnNumber].nNotes[j]; k++)
			{
				str += "  N " + toStr(k) + " ... ";
				str += "\t\tMm:" + toStr(patterns[ptnNumber].notes[k][j].measure);
				str += "\tBt:" + toStr(patterns[ptnNumber].notes[k][j].beat);
				str += "\tDisp:\t" + toStr(patterns[ptnNumber].notes[k][j].displacement);
				str += "\tVel:" + toStr(patterns[ptnNumber].notes[k][j].velocity);
				str += "\r\n";
			}
			str += "\r\n";
		}
		
	return str;
}

std::string DMachine::reportSamples()
{
	string str;
	str = "\r\n";

		
	for(int i=0; i<16; i++)
	{
		str += "Sample No.";
		if(i<10) 
			str+= " ";
		str += toStr(i);
		if(samples[i].loaded)
			str += "\tLoaded ";
		else
			str+=  "\tEmpty  ";
		str += "\t" + samples[i].filename;
		int nSpaces = 28 - samples[i].filename.length();
		for(int j=0; j<nSpaces; j++)
			str += " ";
		str += "Size: " + toStr(samples[i].dataLen);
		str += "\r\n";
	}
	
	str += "\r\n";
		
	return str;
}

void DMachine::start()
{
	playing = true;
}

void DMachine::pause()
{
	playing = false;
}

bool DMachine::isPlaying()
{
	return playing;
}

////////////////


class Audio
{	
	
public:

	static const int SAMPLE_RATE;
	static const int FRAMES_PER_BUFFER;

	DMachine* dmachine;

	PaStreamParameters	inputParameters;
	PaStreamParameters  outputParameters;
	PaStream 			*stream;
	PaError 			err;
	bool				errorRaised;


// constructor
Audio();
~Audio(){}

// real callback function
int audioCallback(
								const void *inputBuffer,
								void *outputBuffer,
								unsigned long framesPerBuffer,
								const PaStreamCallbackTimeInfo* timeInfo,
								PaStreamCallbackFlags statusFlags
							);

// portaudio will call this function on callback. then reroute to real callback func.
static int audioCallbackReroute(
		const 
		void *input, void *output,
		unsigned long frameCount,
		const PaStreamCallbackTimeInfo* timeInfo,
		PaStreamCallbackFlags statusFlags,
		void *userData )
{
		return ((Audio*)userData)
			->audioCallback(input, output, frameCount, timeInfo, statusFlags);
}

void initialize();
void start();
void bindDMachine(DMachine* d);
void terminate();
void raiseError(PaError e);

};




//////////////////////////////////////

const int Audio::SAMPLE_RATE = 44100;
const int Audio::FRAMES_PER_BUFFER = 128;

Audio::Audio()
{
	initialize();
}

// real callback function
int Audio::audioCallback(
								const void *inputBuffer,
								void *outputBuffer,
								unsigned long framesPerBuffer,
								const PaStreamCallbackTimeInfo* timeInfo,
								PaStreamCallbackFlags statusFlags
							)
{
	dmachine->callBackExecuting = true;
	
	int *in = (int*) inputBuffer;
	float *out = (float*)(outputBuffer);
	int nBufferFrames = (int)framesPerBuffer;
	
	(void) timeInfo;
	(void) statusFlags;
	
	
	for(int i=0; i<nBufferFrames; i++)
	{
		//*out = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2)-1.0f);
		*out = dmachine->getMix();
		out++;
	}
	
	dmachine->callBackExecuting = false;
	
	return paContinue;
}

void Audio::initialize()
{	
	errorRaised = false;
	err = Pa_Initialize();
	if( err != paNoError ) raiseError(err);

	inputParameters.device = Pa_GetDefaultInputDevice();
	if(inputParameters.device == paNoDevice)
	{
		cout << "Error: No default input device.\n";
	}
	
	// set up input parameters
	inputParameters.channelCount = 1; // mono input
	inputParameters.sampleFormat = paInt32; // 32-bit integer
	inputParameters.suggestedLatency = Pa_GetDeviceInfo( inputParameters.device )->defaultLowInputLatency;
	inputParameters.hostApiSpecificStreamInfo = NULL;
	
	// set up output parameters
    outputParameters.device = Pa_GetDefaultOutputDevice(); // use default device
    if (outputParameters.device == paNoDevice)
	{
      cout << "Error: No default output device." << "\r\n";
    }
    outputParameters.channelCount = 1; // mono
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;
    
		// 
		//   set up alsa audio latency manually for linux...
		//   default latency somehow doesn't work too well??
		//
    
        #ifdef __linux__
    
		// PaAlsaStreamInfo alsaStreamInfo;
		// PaAlsa_InitializeStreamInfo(&alsaStreamInfo);
		// outputParameters.hostApiSpecificStreamInfo = &alsaStreamInfo;
		
		double defaultLow = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
		double defaultHigh = Pa_GetDeviceInfo( outputParameters.device )->defaultHighOutputLatency;
		string defaultDevName = string( Pa_GetDeviceInfo( outputParameters.device)->name );
		cout << "default device ... " << defaultDevName << endl;
		cout << "default low latency ... " <<  defaultLow << endl;
		cout << "default high latency ... " <<  defaultHigh << endl;

		if(defaultHigh > 0.1)
			outputParameters.suggestedLatency = defaultHigh;
		else
			outputParameters.suggestedLatency = 0.1; // hardcoding the latency amount seems to work well...
    
    #endif
	
	// open audio stream for recording using default device
	err = Pa_OpenStream(
		&stream,
		&inputParameters,
		&outputParameters,
		SAMPLE_RATE,
		FRAMES_PER_BUFFER,
		paClipOff,
		audioCallbackReroute,
		this
	);
	
	if( err != paNoError ) raiseError(err);
	
	cout << "Audio iniialized!\n";
}

void Audio::start()
{
	// start the audio stream!
	err = Pa_StartStream(stream);
	if(err != paNoError) raiseError(err);
}

void Audio::bindDMachine(DMachine* d)
{
	dmachine = d;
}

void Audio::terminate()
{
	err = Pa_CloseStream( stream );
	if(err != paNoError) raiseError(err);	
	err = Pa_Terminate( );
	if(err != paNoError) raiseError(err);	
}

void Audio::raiseError(PaError e)
{
	cout << "Port audio error:\n";
	string errText = Pa_GetErrorText(e);
	cout << errText << "\r\n";
	errorRaised = true;
}





//////////////////////////////////////



////////////////

int main(int argc, char **argv)
{
	// initialize hirolib
	hirolibInit();
	
	// DMachine object
	DMachine dr;
	dr.initialize();
	
	// initialize portaudio
	Audio audio;
	audio.bindDMachine(&dr);

	// get the current file path
	char buffer[512];
	string pwd = string(getcwd(buffer, 512));
	// cout << "Current working directory = " << pwd << "\r\n" << "\r\n";

	// load the samples...
	string resDir = pwd + SH + "res";
	dr.loadAllSamples(resDir);
	
	// load the data file...
	string patternFilePath = resDir + SH + "drum_patterns.txt";
	dr.loadPatterns(patternFilePath);
	dr.setPattern(0);
	
	// get bpm
	int bpm = dr.getBPM();
	
	// re-map pattern 0 according to bpm
	dr.setBPM(bpm);
	dr.remapChannels();
	dr.goToBeginning();
	
	// set up other variables
	float gain = dr.getGain();
	
	// get ready to play...
	dr.start();
	audio.start();
	
	// create sample report of all positions...
	// dr.reportChannels();	

	scr::clear();

	string strMenu = "\r\n*************  Drum Machine! :)  *************\r\n\r\n";
	strMenu += "ESCAPE         ...  Quit\r\n";
	strMenu += "SPACE          ...  Start / stop\r\n";
	strMenu += "LEFT, RIGHT    ...  Choose pattern\r\n";
	strMenu += "U              ...  BPM up\r\n";
	strMenu += "D              ...  BPM down\r\n";
	strMenu += "L              ...  Load drum patterns from file\r\n";
	strMenu += "UP, DOWN       ...  Change volume\r\n";
	strMenu += "M              ...  Display key commands\r\n";
	strMenu += "TAB            ...  Display all patterns\r\n";
	strMenu += "S              ...  DMachine player stats\r\n";
	strMenu += "P              ...  DMachine samples stats\r\n";
	strMenu += "RETURN         ...  DMachine channels stats\r\n\r\n";	
	
	scr::out(strMenu);
	
	bool appExit = false;
	
	while(!appExit)
	{	
		kbd::update();
		
		if(kbd::esc())
		{
			appExit = true;
		}
		// pattern--
		else if(kbd::left())
		{
			int currentPattern = dr.getCurrentPattern();
			currentPattern--;
			if(currentPattern<0)
			{
				currentPattern = dr.getTotalNumberOfPatterns() - 1;
				if(currentPattern<0) currentPattern = 0;
			}
			dr.pause();
			dr.setPattern(currentPattern);
			dr.goToBeginning();
			dr.start();
			cout << "Pattern " << currentPattern << " '" << dr.getCurrentPatternName() << "'" << "\r\n";
		}
		// pattern++
		else if(kbd::right())
		{		
			int currentPattern = dr.getCurrentPattern();
			currentPattern++;
			if(currentPattern>=dr.getTotalNumberOfPatterns())
				currentPattern = 0;
			dr.pause();
			dr.setPattern(currentPattern);
			dr.goToBeginning();
			dr.start();	
			cout << "Pattern " << currentPattern << " '" << dr.getCurrentPatternName() << "'" << "\r\n";			
		}
		// volume up
		else if(kbd::up())
		{
			gain = dr.getGain();
			gain += 0.025f;
			if(gain>1.0f)
				gain = 1.0f;
			dr.setGain(gain);
			cout << "Gain = " << gain << "\r\n";
		}
		// volume down
		else if(kbd::down())
		{
			gain = dr.getGain();
			gain -= 0.025f;
			if(gain<0.0f)
				gain = 0.0f;
			dr.setGain(gain);
			cout << "Gain = " << gain << "\r\n";			
		}
		else if(kbd::u())
		{
			bpm = dr.getBPM();
			if(bpm<300)
				bpm++;
			dr.setBPM(bpm);
			dr.remapChannels();
			cout << "BPM = " << bpm << "\r\n";
		}
		else if(kbd::d())
		{
			bpm = dr.getBPM();
			if(bpm>40)
				bpm--;
			dr.setBPM(bpm);
			dr.remapChannels();
			cout << "BPM = " << bpm << "\r\n";
		}
		else if(kbd::l())
		{
			dr.pause();
			// load the data file...
			string patternFilePath = resDir + SH + "drum_patterns.txt";
			timer::resetTimer();
			dr.loadPatterns(patternFilePath);
			double loadTime = timer::getElapsedTime();
			dr.setPattern(0);
			
			cout << "\r\nLoaded all patterns in " << loadTime << " milliseconds.\r\n\r\n";
			dr.goToBeginning();
		}
		else if(kbd::ret())
		{
			cout << "\r\n" << "\r\n";
	
			int nChannels = dr.getTotalNumberOfChannelsUsed();
			for(int i=0; i<nChannels; i++)
			{
				string strRep = dr.reportChannel(i);
				//
				cout << strRep;

				// wait for key input
				cout << "(Press N for NEXT PAGE)";
				kbd::wait_key_n();
				if(kbd::breakWasRequested())
					i=nChannels;
				cout << "\r\n\r\n";
			}
			cout << "... end of report.\r\n\r\n";
		}
		else if(kbd::space())
		{
			if(dr.isPlaying())
				dr.pause();
			else
				dr.start();
		}
		else if(kbd::m())
		{
			cout << "\r\n";
			cout << strMenu;
		}
		else if(kbd::tab())
		{
			int nPatterns = dr.getTotalNumberOfPatterns();
			for(int i=0; i<nPatterns ; i++)
			{
				cout << dr.reportPattern(i);
				
				// wait for key input
				cout << "(Press N for NEXT PAGE)";
				kbd::wait_key_n();
				if(kbd::breakWasRequested())
					break;
				cout << "\r\n\r\n";
			}
			cout << "... end of report.\r\n\r\n";
		}
		else if(kbd::s())
		{
			cout << dr.reportPlayerStats();	
		}
		else if(kbd::p())
		{
			cout << dr.reportSamples();	
		}
	}
	
	// clean up and exit...
	kbd::flush();
	audio.terminate();
	return 0;
	
}


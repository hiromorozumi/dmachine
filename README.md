# DMachine #

An experimental drum machine app that runs on a command line interface. Note that this is an experimental project. You're welcome to test it out, but it's not yet meant to be a ditribution-ready app.

## Features ##

- Cross-platform (Windows/Mac/Linux)
- Program your drum patterns via text file
- 16 programmable tracks
- Can use user-provided samples
- Variable time signature / measure length

## Contact ##

You're welcome to contact the program author at:
dmachine-at-hiromorozumi-dot-com.
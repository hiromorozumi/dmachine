INCLUDEPATH=-I./include
LIBPATH=-L./lib
LIBS=./lib/libsndfile-1.lib ./lib/portaudio_x86.lib

MACINCLUDEPATH=-I./include -I/usr/local/include
MACLIBPAH=-L/usr/local/lib

LINUXINCLUDEPATH=-I./include -I/usr/local/include
LINUXLIBPATH=-L/usr/local/lib

dmachine:
	g++ dmachine.cpp $(INCLUDEPATH) $(LIBPATH) $(LIBS) -o dmachine
	
debug:
	g++ -g dmachine.cpp $(INCLUDEPATH) $(LIBPATH) $(LIBS) -o dmachine_d

mac:
	g++ dmachine.cpp $(MACINCLUDEPATH) $(MACLIBPATH) -lportaudio -lsndfile -o dmachine_mac
	
macdebug:
	g++ -g dmachine.cpp $(MACINCLUDEPATH) $(MACLIBPATH) -lportaudio -lsndfile -o dmachine_mac_d	

linux:
	g++ dmachine.cpp $(LINUXINCLUDEPATH) $(LINUXLIBPATH) -lportaudio -lsndfile -o dmachine_linux
	
linuxdebug:
	g++ -g dmachine.cpp $(LINUXINCLUDEPATH) $(LINUXLIBPATH) -lportaudio -lsndfile -o dmachine_linux_d	

clean:
	rm -f dmachine.exe
	rm -f dmachine_d.exe
	rm -f ./dmachine_mac
	rm -f ./dmachine_mac_d
	rm -f ./dmachine_linux
	rm -f ./dmachine_linux_d
